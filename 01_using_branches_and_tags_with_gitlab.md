## Objective ##

Learn how gitlab can use branch labels to name build products

## Preparation ##
- fork https://gitlab.com/TonyWildish/gitlab-advanced
- clone your fork to your laptop
- copy **01-gitlab-ci.yml** to **.gitlab-ci.yml**
- edit **.gitlab-ci.yml** amd **change the REGISTRY_USER** variable to your username
- add .gitlab-ci.yml to the git repository, commit the changes, and push them to gitlab
- you should see a pipeline build which produces a **gitlab-advanced:latest** docker image

## Exercise: Branches ##
- create a branch, push it
```
git checkout -b dev
git push --set-upstream origin dev
```

- create another branch, push that too
```
git checkout -b prod
git push --set-upstream origin prod
```

Now check your **Pipelines** and **Registry** tabs in gitlab, you'll see a build for each branch.

You can check out/create as many branches as you want, modify them all separately, they will each build into a separate docker image per branch.

You can run the images directly, e.g.:
```
docker run registry.gitlab.com/<your-username>/gitlab-advanced:dev
```

## Exercise: Tags ##

You can use tags to label images too. From any branch you happen to be on, run this:
```
git tag v0.0.1
git push --tags
git tag v0.0.2
git push --tags
```

Then go to your **Pipelines** and **Registry** pages, see what it does with them.

Gitlab provides many environment variables to a build, you can use them to build the name of the Docker image - or anything else you want to do. Particularly useful are **CI_BUILD_TAG**, which will contain the name of the tag, if any, and **CI_BUILD_REF_NAME**, which contains the name of the tag or the name of the branch. Take a look at the **.gitlab-ci.yml** file to see how it uses them to build the correctly-labeled image.

## Best Practices ##
- use branches for feature-development, then merge the branch back into the main (master) branch when it's complete
- use branches for testing bugfixes, playing around with ideas, doing things without messing up the main lines of development
- use tags to identify particularly noteworthy versions, e.g. releases.