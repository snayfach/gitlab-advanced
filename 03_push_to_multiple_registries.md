## Objective ##

Push a docker image to multiple registries (gitlab, dockerhub, shifter@NERSC...)

## Exercise ##

Copy **03-gitlab-ci.yml** to **.gitlab-ci.yml**, update the **REGISTRY_USER** and commit, push.

Check the **Pipelines** page, you will see a single pipeline being processed.

Check the **Registry** page, you'll see pulldown menus for each container name

You can now pull/run this image from gitlab...:
```
docker pull registry.gitlab.com/<your-username>/gitlab-advanced:latest
```

...or, on Cori/Edison/Denovo:

```
module load shifter # Edison and Denovo only
shifterimg pull registry.services.nersc.gov/<your-username>/gitlab-advanced:latest
shifter run registry.services.nersc.gov/<your-username>/gitlab-advanced:latest
```
N.B. Note that in **03-gitlab-ci.yml** we refer to **registry.services.nersc.gov:8443**, but here we leave the port number off. That's not a typo!

## Best practices ##

- pushing to multiple repositories lets people download their images from the place of their choice. You can have a private repository on gitlab, shared with a team including non-JGI staff, and they can access it there.
- this is also the best way to get an image from a private repository on gitlab to shifter at NERSC. Note that the NERSC shifter repository is NERSC-public, i.e. anyone logged into a NERSC machine can see it.